﻿namespace CameraReader
{
    public class RawHelper
    {
        private int WIDTH;
        private int HEIGHT;
        public RawHelper(int w, int h)
        {
            WIDTH = w;
            HEIGHT = h;
        }

        public byte[] DeBayer(byte[] inp)
        {
            byte[] outp = new byte[WIDTH * HEIGHT * 3];
            float[] inf = new float[WIDTH * HEIGHT];
            float[] outf = new float[WIDTH * 3 * HEIGHT];
            int z;
            for (z = 0; z < WIDTH * HEIGHT; ++z)
            {
                inf[z] = inp[z];
            }

            MalvarDemosaic(ref outf, inf, WIDTH, HEIGHT, 1, 1);
            for (z = 0; z < WIDTH * 3 * HEIGHT; z += 3)
            {
                outp[z] = CLIP(outf[z / 3]);
                outp[z + 1] = CLIP(outf[(z / 3) + (WIDTH * HEIGHT)]);
                outp[z + 2] = CLIP(outf[(z / 3) + (WIDTH * 2 * HEIGHT)]);
            }
            return outp;
        }

        public byte CLIP(float X)
        {
            return (byte)((X) > 255 ? 255 : (X) < 0 ? 0 : X);
        }

        public void MalvarDemosaic(ref float[] Output, float[] Input, int Width, int Height, int RedX, int RedY)
        {
            int BlueX = 1 - RedX;
            int BlueY = 1 - RedY;

            float[][] Neigh = new float[5][];
            int[][] NeighPresence = new int[5][];

            for (int l = 0; l < 5; l++)
            {
                Neigh[l] = new float[5];
                NeighPresence[l] = new int[5];
            }

            int i, j, x, y, nx, ny;

            for (y = 0, i = 0; y < Height; y++)
            {
                for (x = 0; x < Width; x++, i++)
                {
                    for (ny = -2, j = x + Width * (y - 2); ny <= 2; ny++, j += Width)
                    {
                        for (nx = -2; nx <= 2; nx++)
                        {
                            if (0 <= x + nx && x + nx < Width
                                && 0 <= y + ny && y + ny < Height)
                            {
                                Neigh[2 + nx][2 + ny] = Input[j + nx];
                                NeighPresence[2 + nx][2 + ny] = 1;
                            }
                            else
                            {
                                Neigh[2 + nx][2 + ny] = 0;
                                NeighPresence[2 + nx][2 + ny] = 0;
                            }
                        }
                    }

                    if ((x & 1) == RedX && (y & 1) == RedY)
                    {
                        /* Center pixel is red */
                        Output[i] = Input[i];
                        Output[Width * Height + i] = (2 * (Neigh[2][1] + Neigh[1][2]
                            + Neigh[3][2] + Neigh[2][3])
                            + (NeighPresence[0][2] + NeighPresence[4][2]
                                + NeighPresence[2][0] + NeighPresence[2][4]) * Neigh[2][2]
                            - Neigh[0][2] - Neigh[4][2]
                            - Neigh[2][0] - Neigh[2][4])
                            / (2 * (NeighPresence[2][1] + NeighPresence[1][2]
                                + NeighPresence[3][2] + NeighPresence[2][3]));
                        Output[i + 2 * Width * Height] = (4 * (Neigh[1][1] + Neigh[3][1]
                            + Neigh[1][3] + Neigh[3][3]) +
                            3 * ((NeighPresence[0][2] + NeighPresence[4][2]
                                + NeighPresence[2][0] + NeighPresence[2][4]) * Neigh[2][2]
                                - Neigh[0][2] - Neigh[4][2]
                                - Neigh[2][0] - Neigh[2][4]))
                            / (4 * (NeighPresence[1][1] + NeighPresence[3][1]
                                + NeighPresence[1][3] + NeighPresence[3][3]));
                    }
                    else if ((x & 1) == BlueX && (y & 1) == BlueY)
                    {
                        /* Center pixel is blue */
                        Output[i + 2 * Width * Height] = Input[i];
                        Output[Width * Height + i] = (2 * (Neigh[2][1] + Neigh[1][2]
                            + Neigh[3][2] + Neigh[2][3])
                            + (NeighPresence[0][2] + NeighPresence[4][2]
                                + NeighPresence[2][0] + NeighPresence[2][4]) * Neigh[2][2]
                            - Neigh[0][2] - Neigh[4][2]
                            - Neigh[2][0] - Neigh[2][4])
                            / (2 * (NeighPresence[2][1] + NeighPresence[1][2]
                                + NeighPresence[3][2] + NeighPresence[2][3]));
                        Output[i] = (4 * (Neigh[1][1] + Neigh[3][1]
                            + Neigh[1][3] + Neigh[3][3]) +
                            3 * ((NeighPresence[0][2] + NeighPresence[4][2]
                                + NeighPresence[2][0] + NeighPresence[2][4]) * Neigh[2][2]
                                - Neigh[0][2] - Neigh[4][2]
                                - Neigh[2][0] - Neigh[2][4]))
                            / (4 * (NeighPresence[1][1] + NeighPresence[3][1]
                                + NeighPresence[1][3] + NeighPresence[3][3]));
                    }
                    else
                    {
                        /* Center pixel is green */
                        Output[Width * Height + i] = Input[i];

                        if ((y & 1) == RedY)
                        {
                            /* Left and right neighbors are red */
                            Output[i] = (8 * (Neigh[1][2] + Neigh[3][2])
                                + (2 * (NeighPresence[1][1] + NeighPresence[3][1]
                                    + NeighPresence[0][2] + NeighPresence[4][2]
                                    + NeighPresence[1][3] + NeighPresence[3][3])
                                    - NeighPresence[2][0] - NeighPresence[2][4]) * Neigh[2][2]
                                - 2 * (Neigh[1][1] + Neigh[3][1]
                                    + Neigh[0][2] + Neigh[4][2]
                                    + Neigh[1][3] + Neigh[3][3])
                                + Neigh[2][0] + Neigh[2][4])
                                / (8 * (NeighPresence[1][2] + NeighPresence[3][2]));
                            Output[i + 2 * Width * Height] = (8 * (Neigh[2][1] + Neigh[2][3])
                                + (2 * (NeighPresence[1][1] + NeighPresence[3][1]
                                    + NeighPresence[2][0] + NeighPresence[2][4]
                                    + NeighPresence[1][3] + NeighPresence[3][3])
                                    - NeighPresence[0][2] - NeighPresence[4][2]) * Neigh[2][2]
                                - 2 * (Neigh[1][1] + Neigh[3][1]
                                    + Neigh[2][0] + Neigh[2][4]
                                    + Neigh[1][3] + Neigh[3][3])
                                + Neigh[0][2] + Neigh[4][2])
                                / (8 * (NeighPresence[2][1] + NeighPresence[2][3]));
                        }
                        else
                        {
                            /* Left and right neighbors are blue */
                            Output[i] = (8 * (Neigh[2][1] + Neigh[2][3])
                                + (2 * (NeighPresence[1][1] + NeighPresence[3][1]
                                    + NeighPresence[2][0] + NeighPresence[2][4]
                                    + NeighPresence[1][3] + NeighPresence[3][3])
                                    - NeighPresence[0][2] - NeighPresence[4][2]) * Neigh[2][2]
                                - 2 * (Neigh[1][1] + Neigh[3][1]
                                    + Neigh[2][0] + Neigh[2][4]
                                    + Neigh[1][3] + Neigh[3][3])
                                + Neigh[0][2] + Neigh[4][2])
                                / (8 * (NeighPresence[2][1] + NeighPresence[2][3]));
                            Output[i + 2 * Width * Height] = (8 * (Neigh[1][2] + Neigh[3][2])
                                + (2 * (NeighPresence[1][1] + NeighPresence[3][1]
                                    + NeighPresence[0][2] + NeighPresence[4][2]
                                    + NeighPresence[1][3] + NeighPresence[3][3])
                                    - NeighPresence[2][0] - NeighPresence[2][4]) * Neigh[2][2]
                                - 2 * (Neigh[1][1] + Neigh[3][1]
                                    + Neigh[0][2] + Neigh[4][2]
                                    + Neigh[1][3] + Neigh[3][3])
                                + Neigh[2][0] + Neigh[2][4])
                                / (8 * (NeighPresence[1][2] + NeighPresence[3][2]));
                        }
                    }
                }
            }
        }
    }
}
