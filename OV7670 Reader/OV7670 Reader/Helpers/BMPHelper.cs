﻿using System.IO;

namespace CameraReader
{
    public class BMPHelper
    {
        private byte[] Bytes;//Буфер с данными изображения
        private int Width;//Ширина изображения в пикселях
        private int Height;//Высота изображения в пикселях
        private const int ColorDepth = 3;//Глубина цвета. Количество байтов на пиксель
        private const int FileHeaderLength = 14;//Размер заголовка файла
        private const int ImageHeaderLength = 40;//Размер заголовка изображения
        private const int Offset = ImageHeaderLength + FileHeaderLength;//Смещение от начала файла до начала изображения

        /// <summary>
        /// Размер файла с изображением
        /// </summary>
        private int FileSize
        {
            get
            {
                return Offset + ColorDepth * Height * Width;
            }
        }

        /// <summary>
        /// Устанавливает все необходимые заголовки для bmp-формата
        /// </summary>
        private void SetHeader()
        {
            //File header
            Bytes[0] = (byte)'B';
            Bytes[1] = (byte)'M';
            Bytes[2] = (byte)(Bytes.Length >> 24);
            Bytes[3] = (byte)(Bytes.Length >> 16);
            Bytes[4] = (byte)(Bytes.Length >> 8);
            Bytes[5] = (byte)Bytes.Length;
            Bytes[10] = Offset;

            //Image header
            Bytes[14] = ImageHeaderLength;
            Bytes[18] = (byte)Width;
            Bytes[19] = (byte)(Width >> 8);
            Bytes[20] = (byte)(Width >> 16);
            Bytes[21] = (byte)(Width >> 24);
            Bytes[22] = (byte)Height;
            Bytes[23] = (byte)(Height >> 8);
            Bytes[24] = (byte)(Height >> 16);
            Bytes[25] = (byte)(Height >> 24);
            Bytes[26] = 1;
            Bytes[28] = ColorDepth * 8;
        }

        /// <summary>
        /// Превращает двумерный массив "rgbValues" в изображение "{filename.bmp}"
        /// </summary>
        public void SaveBMP(string filename, byte[] rgbValues, int w, int h)
        {
            Height = h;
            Width = w;

            using (FileStream fos = new FileStream(filename, FileMode.Create))
            {
                Bytes = new byte[FileSize];
                SetHeader();
                SaveBitmapData(rgbValues);
                fos.Write(Bytes, 0, FileSize);
            }
        }

        private void SaveBitmapData(byte[] rgbValues)
        {
            int p = 54;

            foreach (byte item in rgbValues)
            {
                Bytes[p++] = item;
            }
        }
    }
}