﻿namespace CameraReader
{
    public class Constants
    {
        public const string COMMAND = "VSYNC";
        public const int WIDTH = 640;
        public const int HEIGHT = 480;
    }
}
