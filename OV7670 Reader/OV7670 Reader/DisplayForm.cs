﻿using System;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;

namespace CameraReader
{
    public partial class DisplayForm : Form
    {
        private SerialPort Port;
        private Boolean IsWorking = true;
        private Byte[] Raw = new byte[Constants.WIDTH * Constants.HEIGHT];
        private RawHelper rawHelper;
        private BMPHelper bmpHelper;

        public DisplayForm()
        {
            InitializeComponent();
        }

        private bool IsVSYNC(int index)
        {
            if (index < Constants.COMMAND.Length)
            {
                if (Constants.COMMAND[index] == Port.ReadChar())
                {
                    return IsVSYNC(++index);
                }
                else
                {
                    return false;
                }
            }
            return true;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            foreach (string port in SerialPort.GetPortNames())
            {
                comboBox1.Items.Add(port);
            }

            rawHelper = new RawHelper(Constants.WIDTH, Constants.HEIGHT);
            bmpHelper = new BMPHelper();
        }

        private void comboBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            string comPortName = comboBox1.SelectedItem.ToString();
            if (comPortName.Length > 0)
            {
                try
                {
                    Port = new SerialPort(comPortName, 1000000, Parity.None, 8, StopBits.One);
                    Port.Open();
                    new Thread(() =>
                    {
                        try
                        {
                            int counter = 0;

                            while (IsWorking)
                            {
                                while (!IsVSYNC(0)) { };
                                int ind = 0;
                                for (int y = 0; y < Constants.HEIGHT; y++)
                                {
                                    for (int x = 0; x < Constants.WIDTH; x++)
                                    {
                                        try
                                        {
                                            int curByte = Port.ReadChar();
                                            Raw[ind++] = (byte)curByte;
                                        }
                                        catch
                                        {
                                            IsWorking = false;
                                            Thread.CurrentThread.Abort();
                                            break;
                                        }
                                    }
                                }

                                string fileName = string.Format("{0}.bmp", counter++);

                                bmpHelper.SaveBMP(fileName, rawHelper.DeBayer(Raw), Constants.WIDTH, Constants.HEIGHT);
                                pictureBox1.Load(fileName);
                            }

                            Dispose();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }).Start();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void Dispose()
        {
            Port.Dispose();
            pictureBox1.Dispose();
            if (!checkBox1.Checked)
            {
                foreach (string file in Directory.GetFiles(GetLocation(), "*.bmp"))
                {
                    File.Delete(file);
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            IsWorking = false;
        }

        public static string GetLocation()
        {
            string[] nodes = Assembly.GetEntryAssembly().Location.Split('\\');
            string path = string.Empty;
            for (int i = 0; i < nodes.Length - 1; i++)
            {
                path += nodes[i] + '\\';
            }
            return path;
        }
    }
}