﻿using CameraReader;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using WebSite.Models;

namespace WebSite.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            BMPHelper bmpHelper = new BMPHelper();
            Reader reader = new Reader();
            reader.Start();

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public string Image(string name)
        {
            byte[] fileBytes = System.IO.File.ReadAllBytes(HostingEnvironment.MapPath(@"~/App_Data/") + name);
            return Convert.ToBase64String(fileBytes);
        }
    }
}