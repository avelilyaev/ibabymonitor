﻿using CameraReader;
using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Web;
using System.Web.Hosting;
using WebSite.Hubs;

namespace WebSite.Models
{
    public class Reader
    {
        private const string COMMAND = "RDY";
        private const int WIDTH = 640;
        private const int HEIGHT = 480;
        private int[][] rgb;
        private int[][] rgb2;
        SerialPort port;
        private bool IsWorking = true;
        private List<string> ports = new List<string>();

        public int counter = 0;

        private bool IsImageStart(int index)
        {
            if (index < COMMAND.Length)
            {
                if (COMMAND[index] == port.ReadChar())
                {
                    return IsImageStart(++index);
                }
                else
                {
                    return false;
                }
            }
            return true;
        }

        public Reader()
        {
            foreach (string port in SerialPort.GetPortNames())
            {
                ports.Add(port);
            }

            rgb = new int[HEIGHT][];
            for (int i = 0; i < rgb.Length; i++)
            {
                rgb[i] = new int[WIDTH];
            }

            rgb2 = new int[WIDTH][];
            for (int i = 0; i < rgb2.Length; i++)
            {
                rgb2[i] = new int[HEIGHT];
            }
        }

        public static string GetLocation()
        {
            string[] nodes = Assembly.GetEntryAssembly().Location.Split('\\');
            string path = string.Empty;
            for (int i = 0; i < nodes.Length - 1; i++)
            {
                path += nodes[i] + '\\';
            }
            return path;
        }

        private void Dispose()
        {
            port.Dispose();
        }

        public void Start()
        {
            string comPortName = ports.Last();
            if (comPortName.Length > 0)
            {
                try
                {
                    if (port == null)
                    {
                        port = new SerialPort(comPortName, 1000000, Parity.None, 8, StopBits.One);
                        port.Open();
                        new Thread(() =>
                        {
                            try
                            {
                                while (IsWorking)
                                {
                                    while (!IsImageStart(0)) { };
                                    for (int y = 0; y < HEIGHT; y++)
                                    {
                                        for (int x = 0; x < WIDTH; x++)
                                        {
                                            try
                                            {
                                                int temp = port.ReadChar();
                                                rgb[y][x] = ((temp & 0xFF) << 16) | ((temp & 0xFF) << 8) | (temp & 0xFF);
                                            }
                                            catch
                                            {
                                                IsWorking = false;
                                                Thread.CurrentThread.Abort();
                                                break;
                                            }
                                        }
                                    }
                                    for (int y = 0; y < HEIGHT; y++)
                                    {
                                        for (int x = 0; x < WIDTH; x++)
                                        {
                                            rgb2[x][y] = rgb[y][x];
                                        }
                                    }
                                    BMPHelper bmp = new BMPHelper();
                                    string path = HostingEnvironment.MapPath(@"~/App_Data/");
                                    string name = counter + ".bmp";

                                    //bmp.SaveBMP(path + name, rgb2, WIDTH, HEIGHT);
                                    var hubContext = GlobalHost.ConnectionManager.GetHubContext<CameraHub>();
                                    hubContext.Clients.All.hello(name);

                                    counter++;
                                }

                                Dispose();
                            }
                            catch (Exception ex)
                            {
                            //MessageBox.Show(ex.Message);
                        }
                        }).Start();
                    }
                }
                catch (Exception ex)
                {
                    //MessageBox.Show(ex.Message);
                }
            }
        }
    }
}