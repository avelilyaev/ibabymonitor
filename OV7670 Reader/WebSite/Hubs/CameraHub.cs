﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System.Web.Routing;

namespace WebSite.Hubs
{
    [HubName("cameraHub")]
    public class CameraHub : Hub
    {
        public void Hello(string name)
        {
            Clients.All.hello(name);
        }
    }
}