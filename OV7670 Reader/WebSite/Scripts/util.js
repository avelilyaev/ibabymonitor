﻿jQuery(document).ready(function () {
    // Ссылка на автоматически-сгенерированный прокси хаба
    var hub = $.connection.cameraHub;

    // Объявление функции, которая хаб вызывает при получении сообщений
    hub.client.hello = function (filename) {
        console.log(filename);
        debugger;

        $.ajax({
            url: 'Home/Image?name=' + filename,
            success: function (data) {
                var image = "data:image/jpeg;base64," + data;
                $("img").attr("src", image);
            },
            error : function (er)
            {
                debugger;
            }
        });
    };

    // Открываем соединение
    $.connection.hub.start().done(function () {

    });
});
